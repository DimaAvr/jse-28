package ru.tsc.avramenko.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.command.AbstractCommand;
import ru.tsc.avramenko.tm.model.Command;

import java.util.*;
import java.util.stream.Collectors;

public class HelpCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "help";
    }

    @Nullable
    @Override
    public String arg() {
        return "-h";
    }

    @NotNull
    @Override
    public String description() {
        return "Display list of commands.";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        int index = 1;
        final HashMap<String, String> cmd = new HashMap<>();
        for (final AbstractCommand command : serviceLocator.getCommandService().getCommands()) {
            cmd.put(command.toString(), command
                    .getClass()
                    .getPackage()
                    .getName()
                    .substring(command.getClass().getPackage().getName().lastIndexOf(".")+1).toUpperCase());
        }
        final Map<String, List<String>> valueMap = cmd.keySet().stream().collect(Collectors.groupingBy(k -> cmd.get(k)));
        for(String s : valueMap.keySet()) {
            System.out.println("\n" + s + " COMMAND:");
            for (String str : valueMap.get(s)) {
                System.out.println(index + ". " + str);
                index++;
            }
        }
    }

}